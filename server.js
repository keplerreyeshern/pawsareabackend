'use strict'

const express = require('express');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.get('/', (req, res) => {
    res.send('HOLA MUNDO\n');
});

app.listen(PORT, HOST);
console.log(`Ronnung on http://${HOST}:${PORT}`);
