FROM node:8-alpine

WORKDIR /user/src/app
COPY package.json ./
RUN npm install -g nodemon
RUN npm install
COPY . .
EXPOSE 8080
CMD ["npm", "start"]
